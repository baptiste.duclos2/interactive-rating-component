# Frontend Mentor - Product preview card component solution

This is a solution to the [Product preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/product-preview-card-component-GO7UmttRfa). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover and focus states for interactive elements
- Pick a rating and submit it
- See the hidden part of the project once the score submited

### Screenshot
### DESKTOP VERSION
![desktop](screenshots/desktop1.png)
![desktop](screenshots/desktop2.png)
### MOBILE VERSION
![mobile](screenshots/mobile1.png)
![mobile](screenshots/mobile2.png)


## My process

- I started being more confident with the basics of HTML and CSS
- I did the mobile-first process
- Once everything was perfect in html/CSS i started learning Jquery
- cut every part of the review system in Jquery

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- javascript
- Jquery


### What I learned

I have decided to learn Jquery with javascript since i had a lot of experience with javascript but nothing in Jquery and i wanted to try a JS library in order
to have higher chances of getting a job later.
I am very proud of the code i've managed to do in order to get the perfect rating system for me :

```js
// Switch the number color on click and gives back old css to last clicked number
    $(".number").on("click", function(){
        lastFontColor = $(this).children().css("color");
        $(this).css("background-color", "#FC7614");
        $(this).children().css("color", "#FFFFFF");
        // If not the first number selected, change the last object to it's old CSS
        if (lastIndex >= 0){
            $('.number').eq(lastIndex).css("background-color", "#262E38");
            $('.number').eq(lastIndex).children().css("color", "#7C8798");
        }
        //get this object as the last object
        lastIndex = ($(".number").index($(this)));
    })
```

I had a lot of struggles in the first place in order to figure out how to get the last object, but once i got it, it was way way easier than expected

### Continued development

In future projects, I'll spend more time on the undestanding of Jquery so that I will no longer need any search on the internet.

