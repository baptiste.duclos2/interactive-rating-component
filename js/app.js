$(document).ready(function(){
    // variable for last nth
    let lastIndex = -1;

    //variable for score
    let score = -1;

    // hides the attribution for test
    $(".attribution").hide();
    $("#feedback").hide();

    // Switch the number color on click and gives back old css to last clicked number
    $(".number").on("click", function(){
        lastFontColor = $(this).children().css("color");
        $(this).css("background-color", "#FC7614");
        $(this).children().css("color", "#FFFFFF");
        // If not the first number selected, change the last object to it's old CSS
        if (lastIndex >= 0){
            $('.number').eq(lastIndex).css("background-color", "#262E38");
            $('.number').eq(lastIndex).children().css("color", "#7C8798");
        }
        //get this object as the last object
        lastIndex = ($(".number").index($(this)));
    })

    // Fonction to submit the score
    $("#submit").on("click", function(){
        if (lastIndex != -1){
            // get lastIndex +1 as the score
            score = lastIndex + 1;
            // Writes the score text
            $("#selection").html(`You selected ${score} out of 5`);
            // Hides the current div
            $("#scoring").hide();
            // Show the hidden div
            $("#feedback").show();
        }
        else{
            alert(" ! Warning ! You did not enter a score !");
        }
    })
})
